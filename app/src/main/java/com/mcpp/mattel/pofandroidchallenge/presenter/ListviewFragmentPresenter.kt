package com.mcpp.mattel.pofandroidchallenge.presenter

import android.util.Log
import com.mcpp.mattel.pofandroidchallenge.repo.IPOFRepository
import com.mcpp.mattel.pofandroidchallenge.model.ServerResponse
import com.mcpp.mattel.pofandroidchallenge.presenter.interfaces.IListviewFragmentPresenter
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IListViewFragment
import com.mcpp.mattel.pofandroidchallenge.view.ListviewFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */
class ListviewFragmentPresenter(
    val mView: IListViewFragment,
    val mRepo: IPOFRepository
) : IListviewFragmentPresenter {
    override fun getServerResponse() {
        mRepo.getCommitResponse(object : Callback<ServerResponse> {
            override fun onResponse(
                call: Call<ServerResponse>,
                response: Response<ServerResponse>
            ) {
                val commitList = response.body()!!
                mView.setServerValuesToAdapter(commitList)
            }

            override fun onFailure(call: Call<ServerResponse>, t: Throwable) {
                // Log error here since request failed
                Log.e(ListviewFragment.TAG, t.toString())
                mView.showError()
            }
        })
    }


}