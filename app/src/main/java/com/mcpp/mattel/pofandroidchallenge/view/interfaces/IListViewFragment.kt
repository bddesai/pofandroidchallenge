package com.mcpp.mattel.pofandroidchallenge.view.interfaces

import com.mcpp.mattel.pofandroidchallenge.model.ServerResponseItem


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */
interface IListViewFragment {

    fun setServerValuesToAdapter(commitList: List<ServerResponseItem>)

    fun showError()
}