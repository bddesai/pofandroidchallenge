package com.mcpp.mattel.pofandroidchallenge.repo

import com.mcpp.mattel.pofandroidchallenge.model.ServerResponse
import com.mcpp.mattel.pofandroidchallenge.service.ApiClient
import retrofit2.Call
import retrofit2.Callback


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */
class POFRepository : IPOFRepository {
    override fun getCommitResponse(callback: Callback<ServerResponse>) {
        val apiService: ApiClient = ApiClient.create()
        var call: Call<ServerResponse> = apiService.getCommits()
        call.enqueue(callback)
    }


}