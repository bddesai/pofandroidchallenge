package com.mcpp.mattel.pofandroidchallenge.service

import com.mcpp.mattel.pofandroidchallenge.model.ServerResponse
import com.mcpp.mattel.pofandroidchallenge.utils.Constants
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


/**
 * Created by Bhavin Desai on 27 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */


interface ApiClient {
    // List of API endpoints
    @GET("commits")
    fun getCommits(): Call<ServerResponse>

    //create Retrofit REST client
    companion object Factory {
        fun create(): ApiClient {
            // Generate a Retrofit Request
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()

            return retrofit.create(ApiClient::class.java)
        }
    }
}