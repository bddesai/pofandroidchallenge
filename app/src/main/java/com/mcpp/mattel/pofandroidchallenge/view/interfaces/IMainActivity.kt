package com.mcpp.mattel.pofandroidchallenge.view.interfaces

import android.os.Bundle

interface IMainActivity {
    fun fragmentInteractionListener(bundle: Bundle)
}
