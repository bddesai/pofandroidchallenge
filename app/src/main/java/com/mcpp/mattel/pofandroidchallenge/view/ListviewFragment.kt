package com.mcpp.mattel.pofandroidchallenge.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import com.mcpp.mattel.pofandroidchallenge.repo.POFRepository
import com.mcpp.mattel.pofandroidchallenge.R
import com.mcpp.mattel.pofandroidchallenge.model.ServerResponseItem
import com.mcpp.mattel.pofandroidchallenge.presenter.ListviewFragmentPresenter
import com.mcpp.mattel.pofandroidchallenge.utils.Constants
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IListViewFragment
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IMainActivity
import org.jetbrains.anko.toast


/**
 * A simple [Fragment] subclass.
 */
class ListviewFragment : Fragment(), IListViewFragment {

    companion object {
        val TAG = ListviewFragment::class.java.simpleName
    }

    private lateinit var listviewFragmentPresenterImpl: ListviewFragmentPresenter

    private lateinit var listView: ListView
    private var mCommitList: List<ServerResponseItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listviewFragmentPresenterImpl = ListviewFragmentPresenter(this, POFRepository())

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_listview, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView = view.findViewById(R.id.listView)
        listviewFragmentPresenterImpl.getServerResponse()
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                val selectedItem = mCommitList?.get(position)
                val bundle = Bundle()
                bundle.putString(Constants.DATE, selectedItem?.commit?.committer?.date)
                bundle.putString(Constants.MESSAGE, selectedItem?.commit?.message)
                bundle.putString(Constants.LOGIN, selectedItem?.author?.login)
                bundle.putString(Constants.SHA, selectedItem?.sha)
                bundle.putString(Constants.AVATAR_URL, selectedItem?.author?.avatar_url)
                bundle.putString(Constants.HTML_URL, selectedItem?.author?.html_url)

                (activity as IMainActivity).fragmentInteractionListener(bundle)

                Log.d(TAG, "Selected Item: $selectedItem")
            }
    }

    override fun setServerValuesToAdapter(commitList: List<ServerResponseItem>) {
        mCommitList = commitList
        listView.adapter = activity?.baseContext?.let { CommitListAdapter(it, commitList) }
    }

    override fun showError() {
        activity?.toast(R.string.service_error_message)
    }

}