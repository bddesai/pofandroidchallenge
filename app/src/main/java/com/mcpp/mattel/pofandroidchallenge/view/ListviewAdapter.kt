package com.mcpp.mattel.pofandroidchallenge.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.ViewFlipper
import com.bumptech.glide.Glide
import com.mcpp.mattel.pofandroidchallenge.R
import com.mcpp.mattel.pofandroidchallenge.model.ServerResponseItem
import com.mcpp.mattel.pofandroidchallenge.utils.Utils


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */

class CommitListAdapter(context: Context, dataList: List<ServerResponseItem>) : BaseAdapter() {

    private var sList = dataList
    private val context = context
    private val mInflator: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return sList.size
    }

    override fun getItem(position: Int): Any {
        return sList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        val view: View?
        val vh: ListRowHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.list_row, parent, false)
            vh = ListRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ListRowHolder
        }

        val dateString = sList[position].commit.committer.date
        vh.label_rest_date.text = Utils.convertDateTime(dateString)
        vh.label_rest_message.text = sList[position].commit.message
        vh.label_rest_login.text = sList[position].author.login
        Glide.with(context)
            .load(sList[position].author.avatar_url)
            .into(vh.image_rest_avatar)
        return view
    }
}

class ListRowHolder(row: View?) {
    val label_rest_date: TextView = row?.findViewById(R.id.label_rest_date) as TextView
    val label_rest_login: TextView = row?.findViewById(R.id.label_rest_login) as TextView
    val label_rest_message: TextView = row?.findViewById(R.id.label_rest_message) as TextView
    val image_rest_avatar: ImageView = row?.findViewById(R.id.image_rest_avatar) as ImageView

    val viewFlipper: ViewFlipper = row?.findViewById(R.id.viewFlipper1) as ViewFlipper
}
