package com.mcpp.mattel.pofandroidchallenge.view

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcpp.mattel.pofandroidchallenge.R
import com.mcpp.mattel.pofandroidchallenge.presenter.DetailFragmentPresenter
import com.mcpp.mattel.pofandroidchallenge.utils.Constants
import com.mcpp.mattel.pofandroidchallenge.utils.Utils
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IDetailFragment


/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : BottomSheetDialogFragment(), IDetailFragment {
    companion object {
        private val TAG = DetailFragment::class.java.simpleName
        fun instance(bundle: Bundle): DetailFragment {
            val detailFragment = DetailFragment().apply {
                arguments = bundle
            }
            return detailFragment
        }
    }

    private lateinit var detailFragmentPresenterImpl: DetailFragmentPresenter


    private var imageViewAvatar: ImageView? = null
    private var txtDate: TextView? = null
    private var txtLogin: TextView? = null
    private var txtSha: TextView? = null
    private var txtCommitMessage: TextView? = null
    private var btnCloseView: ImageView? = null

    private var htmlUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailFragmentPresenterImpl = DetailFragmentPresenter(this)
    }

    override fun setupDialog(dialog: Dialog, style: Int) {

        setStyle(DialogFragment.STYLE_NORMAL, R.style.MyCustomTheme)
        // setup bottom sheet dialog behavior
        val contentView = View.inflate(context, R.layout.bottom_sheet_layout, null)
        dialog.setContentView(contentView)
        val layoutParams =
            (contentView.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        val behavior = layoutParams.behavior
        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            behavior.addBottomSheetCallback(bottomSheetBehaviorCallback)
            behavior.isHideable = false
            behavior.setPeekHeight(250, true)
        }


        // setup views
        imageViewAvatar = contentView.findViewById<View>(R.id.detail_avatar_image) as ImageView
        txtDate = contentView.findViewById<View>(R.id.detail_date) as TextView
        txtLogin = contentView.findViewById<View>(R.id.detail_login) as TextView
        txtSha = contentView.findViewById<View>(R.id.detail_sha) as TextView
        txtCommitMessage = contentView.findViewById<View>(R.id.detail_commit_message) as TextView

        btnCloseView = contentView.findViewById<View>(R.id.btn_close_view) as ImageView
        btnCloseView!!.setOnClickListener { dismiss() }


        arguments?.let {
            activity?.baseContext?.let { it1 ->
                Glide.with(it1)
                    .load(it.getString(Constants.AVATAR_URL))
                    .into(imageViewAvatar!!)
            }

            htmlUrl = it.getString(Constants.HTML_URL)
            txtDate!!.text = it.getString(Constants.DATE)?.let { it1 -> Utils.convertDateTime(it1) }
            txtLogin!!.text = it.getString(Constants.LOGIN)
            txtSha!!.text = it.getString(Constants.SHA)
            txtCommitMessage!!.text = it.getString(Constants.MESSAGE)
        }

        imageViewAvatar!!.setOnClickListener { htmlUrl?.let { it1 -> openUrl(it1) } }
    }

    fun openUrl(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    private fun setCellColor(newState: Int) {
        //dialogStateText!!.setText(BottomSheetActivity.getStateAsString(newState))
    }

    private val bottomSheetBehaviorCallback: BottomSheetBehavior.BottomSheetCallback =
        object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismiss()
                }
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    setCellColor(newState)
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //
            }
        }

}