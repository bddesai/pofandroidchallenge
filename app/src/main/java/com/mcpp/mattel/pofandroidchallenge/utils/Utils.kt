package com.mcpp.mattel.pofandroidchallenge.utils

import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */

class Utils {
    companion object {
        fun convertDateTime(datetime: String): String {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
            val outputFormat = SimpleDateFormat("MM-dd-yyyy", Locale.US)
            val date: Date = inputFormat.parse(datetime)
            return outputFormat.format(date)
        }
    }
}