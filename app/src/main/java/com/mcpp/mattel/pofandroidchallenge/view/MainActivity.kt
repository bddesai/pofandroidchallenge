package com.mcpp.mattel.pofandroidchallenge.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcpp.mattel.pofandroidchallenge.R
import com.mcpp.mattel.pofandroidchallenge.presenter.ActivityPresenter
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IMainActivity

class MainActivity : AppCompatActivity(), IMainActivity {

    private var TAG: String = this@MainActivity::class.java.simpleName
    private lateinit var activityPresenterImpl: ActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityPresenterImpl = ActivityPresenter(this)



        if (savedInstanceState == null) { // initial transaction should be wrapped like this
            supportFragmentManager.beginTransaction()
                .replace(R.id.root_container, ListviewFragment())
                .commit()
        }
    }

    override fun fragmentInteractionListener(bundle: Bundle) {
        val dialogFragment: BottomSheetDialogFragment = DetailFragment.instance(bundle)
        dialogFragment.show(supportFragmentManager, dialogFragment.tag)
    }


}