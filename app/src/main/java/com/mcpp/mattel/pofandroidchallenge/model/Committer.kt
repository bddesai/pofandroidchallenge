package com.mcpp.mattel.pofandroidchallenge.model

data class Committer(
    val date: String,
    val email: String,
    val name: String
)