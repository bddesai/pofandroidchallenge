package com.mcpp.mattel.pofandroidchallenge.model

data class Tree(
    val sha: String,
    val url: String
)