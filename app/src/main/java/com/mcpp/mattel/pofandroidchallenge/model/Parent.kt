package com.mcpp.mattel.pofandroidchallenge.model

data class Parent(
    val html_url: String,
    val sha: String,
    val url: String
)