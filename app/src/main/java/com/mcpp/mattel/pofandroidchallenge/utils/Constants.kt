package com.mcpp.mattel.pofandroidchallenge.utils


/**
 * Created by Bhavin Desai on 27 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */

class Constants {
    companion object {

        val BASE_URL = "https://api.github.com/repos/definitelytyped/definitelytyped/"

        fun isReallyNull(string: String): Boolean {
            if (string.isNullOrEmpty() || string.isNullOrBlank()) {
                return true
            }
            return false
        }

        val DATE = "DATE"
        val MESSAGE = "MESSAGE"
        val LOGIN = "LOGIN"
        val AVATAR_URL = "AVATAR_URL"
        val HTML_URL = "HTML_URL"
        val SHA = "SHA"
    }
}
