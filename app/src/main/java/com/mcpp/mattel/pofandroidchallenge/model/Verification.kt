package com.mcpp.mattel.pofandroidchallenge.model

data class Verification(
    val payload: String,
    val reason: String,
    val signature: String,
    val verified: Boolean
)