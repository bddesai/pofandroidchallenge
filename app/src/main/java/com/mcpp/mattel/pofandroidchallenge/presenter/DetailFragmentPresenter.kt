package com.mcpp.mattel.pofandroidchallenge.presenter

import com.mcpp.mattel.pofandroidchallenge.presenter.interfaces.IDetailFragmentPresenter
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IDetailFragment


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */
class DetailFragmentPresenter(val mView: IDetailFragment): IDetailFragmentPresenter {
}