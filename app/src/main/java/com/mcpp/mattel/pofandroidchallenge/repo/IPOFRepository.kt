package com.mcpp.mattel.pofandroidchallenge.repo

import com.mcpp.mattel.pofandroidchallenge.model.ServerResponse
import retrofit2.Callback

interface IPOFRepository {
    fun getCommitResponse(callback: Callback<ServerResponse>)
}
