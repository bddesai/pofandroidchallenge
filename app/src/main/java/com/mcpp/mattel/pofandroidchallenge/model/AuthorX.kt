package com.mcpp.mattel.pofandroidchallenge.model

data class AuthorX(
    val date: String,
    val email: String,
    val name: String
)