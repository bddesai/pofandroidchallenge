package com.mcpp.mattel.pofandroidchallenge.presenter

import com.mcpp.mattel.pofandroidchallenge.presenter.interfaces.IActivityPresenter
import com.mcpp.mattel.pofandroidchallenge.view.interfaces.IMainActivity


/**
 * Created by Bhavin Desai on 28 September,2020
 * Copyright © 2020 Mattel Inc. All rights reserved.
 */

class ActivityPresenter(var mView: IMainActivity) : IActivityPresenter {

}